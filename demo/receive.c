#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

struct my_msgbuf {
	long mtype;
	char mtext[200];
};

int calculate(char mtext[200]) {
	int result = 0;
	char number_1[20];
	char number_2[20];
	char operator;
	int pos = 0;

	for (int i = 0; i < strlen(mtext); i++) {
		if (mtext[i] == '+' || mtext[i] == '-' || mtext[i] == '*' || mtext[i] == '/') {
			operator = mtext[i];
			pos = i + 2;
			break;
		}
		number_1[i] = mtext[i];
	}

	number_1[pos-3] = '\0';

	for (int j = pos; j <= strlen(mtext); j++) {
		number_2[j - pos] = mtext[j]; 
	}
	
	switch(operator) {
		case '+':
			result = atoi(number_1) + atoi(number_2);
			break;
		case '-':
			result = atoi(number_1) - atoi(number_2);
			break;
		case '*':
			result = atoi(number_1) * atoi(number_2);
			break;
		case '/':
			result = atoi(number_1) / atoi(number_2);
			break;
	}

	return result;
}

int main() {
	struct my_msgbuf buf;
	int msqid;
	key_t key;

	if ((key = ftok("send.c", 'B')) == -1) {
		perror("ftok");
		exit(1);
	}

	if ((msqid = msgget(key, 0777 | IPC_CREAT)) == -1) {
		perror("msgget");
		exit(1);
	}
	
	printf("Ready to receive messages...\n");

	for(;;) {
		if (msgrcv(msqid, &buf, sizeof buf.mtext, 0, 0) == -1) {
			perror("msgrcv");
			exit(1);
		}
		int result = calculate(buf.mtext);
		printf("%s = %d\n", buf.mtext, result);
	}

	return 0;
}

